# GitHub Follow

This program automates two activities on your GitHub account:

- Following users who start following you  
- Unfollowing users who stop following you  

The program is designed to run on a server or device with a permanent internet connection. These actions are executed every 1 hour.

## Step-by-Step Instructions

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/mateolafalce/github-follow.git
   ```

2. Navigate to the project directory:
   ```bash
   cd github-follow/
   ```

3. Set your GitHub username and token in `src/main.rs`:
   ```rust
   const USERNAME: &str = "my_username";
   const TOKEN: &str = "my_token";
   ```

4. Compile the project. If you want to run the program on an x86-64 PC, simply execute:
   ```bash
   cargo run &
   ```

5. If you want to run the program on another platform, such as an Android phone with [Termux](https://f-droid.org/en/packages/com.termux/), compile the project using [cross](https://github.com/cross-rs/cross):
   ```bash
   cross build --target armv7-linux-androideabi
   ```

6. If you encounter any issues with `docker.sock`, you may need to adjust its permissions. This command resolved the issue for me:
   ```bash
   sudo chmod 666 /var/run/docker.sock
   ```

7. Once compiled, compress the `github-follow` directory:
   ```bash
   zip -r github-follow.zip github-follow/
   ```

8. Transfer the `github-follow.zip` file to the Termux SSH server:
   ```bash
   scp -P 8022 github-follow.zip <user>@<ip>:
   ```

9. Unzip the file on the target device:
   ```bash
   unzip github-follow.zip
   ```

10. Then you can set a crontab for the program
    ```bash
     touch run_github.sh && chmod +x run_github.sh
    ```
    ```bash
     #!/bin/bash
     cd ~/github-follow/target/armv7-linux-androideabi/debug/ && ./github-follow
    ```
    ```bash
     crontab -e
    ```
    ```bash
     0 * * * * ~/run_github.sh
    ```

If it's your first time using crontab in Termux you must reboot the system. See more [here](https://www.reddit.com/r/termux/comments/i27szk/how_do_i_crontab_on_termux/)

Now your GitHub account will automatically follow and unfollow users as needed, keeping it active and up-to-date.
