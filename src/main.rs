/***************************************************************************************
 *   main.rs  --  This file is part of who_doesnt_follow_me.                           *
 *                                                                                     *
 *   Copyright (C) 2025 Mateo Lafalce                                                  *
 *                                                                                     *
 *   who_doesnt_follow_me is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   who_doesnt_follow_me is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

mod io;
mod request;
mod user;

use crate::{
    io::followers_diff::followers_diff,
    request::get_users_from_account::{get_users_from_account, TypeUser},
    user::User,
};
use anyhow::Result;
use tracing::info;

/// Constant used to set the Github username
/// Example: mateolafalce
const USERNAME: &str = "";
/// Constant used to set the Github token
/// Example: ghp_abcdefghijklmnñopqrstuvxyzABCDEFGHIJ
const TOKEN: &str = "";

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .init();
    let user: User = User::new(USERNAME, TOKEN);
    let users_i_follow: &Vec<String> = &get_users_from_account(TypeUser::Following, &user).await?;
    let users_follow_me: &Vec<String> = &get_users_from_account(TypeUser::Follower, &user).await?;
    followers_diff(
        users_i_follow,
        users_follow_me,
        &user,
        "Don't follow you",
        true,
    )
    .await;
    followers_diff(
        users_follow_me,
        users_i_follow,
        &user,
        "You don't follow",
        false,
    )
    .await;
    info!("Process finished");
    Ok(())
}
