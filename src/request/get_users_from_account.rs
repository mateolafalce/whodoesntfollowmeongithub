/***************************************************************************************
 *   get_users_from_account.rs  --  This file is part of who_doesnt_follow_me.         *
 *                                                                                     *
 *   Copyright (C) 2025 Mateo Lafalce                                                  *
 *                                                                                     *
 *   who_doesnt_follow_me is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   who_doesnt_follow_me is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

use crate::user::User;
use anyhow::Result;
use octorust::{auth::Credentials, Client};
use tracing::info;

pub enum TypeUser {
    Following,
    Follower,
}

/// This program can return two types of vectors with usernames depending on the case:
/// `Follower` => Vec<usernames of people who follow me>
/// `Following` => Vec<usernames of people I follow>
pub async fn get_users_from_account(type_user: TypeUser, user: &User) -> Result<Vec<String>> {
    let token: Credentials = Credentials::Token(user.token.to_string());
    let client = Client::new("User", token)?;
    let users_request = match type_user {
        TypeUser::Follower => {
            info!("Getting Users that Follow You ...");
            client.users().list_all_followers_for_user(user.name).await
        }
        TypeUser::Following => {
            info!("Getting Users You Follow ...");
            client.users().list_all_following_for_user(user.name).await
        }
    };

    let mut vec_users: Vec<String> = [].to_vec();
    let response = users_request?;
    for user in response.body {
        vec_users.push(user.login);
    }
    Ok(vec_users)
}
