/***************************************************************************************
 *   show_users_dont_follow.rs  --  This file is part of who_doesnt_follow_me.         *
 *                                                                                     *
 *   Copyright (C) 2025 Mateo Lafalce                                                  *
 *                                                                                     *
 *   who_doesnt_follow_me is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published                 *
 *   by the Free Software Foundation, either version 3 of the License,                 *
 *   or (at your option) any later version.                                            *
 *                                                                                     *
 *   who_doesnt_follow_me is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty                       *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                           *
 *   See the GNU General Public License for more details.                              *
 *                                                                                     *
 *   You should have received a copy of the GNU General Public License                 *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.               *
 *                                                                                     *
 **************************************************************************************/

use crate::user::User;
use octorust::{auth::Credentials, Client};
use std::{thread::sleep, time::Duration};
use tracing::{error, info, warn};

/// Time that the screen sleeps between log and log.
/// It is represented in milliseconds.
const TIME: u64 = 200;

/// Depending on the order in which the `Vec` of usernames (users I follow / users who follow me) is passed, we will get the following:  
/// - `[0]`: Follow those who follow me but whom I currently don't follow.  
/// - `[1]`: Unfollow those who stopped following me.
pub async fn followers_diff(
    users_i_follow: &[String],
    users_follow_me: &[String],
    user: &User,
    custom_msg: &str,
    i_need_to_follow_him: bool,
) {
    let token: Credentials = Credentials::Token(user.token.to_string());
    let client = Client::new("User", token).expect("invalid");
    let difference: Vec<_> = users_i_follow
        .iter()
        .filter(|&elem| !users_follow_me.contains(elem))
        .collect();
    for user in difference {
        if i_need_to_follow_him {
            sleep(Duration::from_millis(TIME));
            warn!("{} https://github.com/{}", custom_msg, user);
            sleep(Duration::from_millis(TIME));
            match client.users().unfollow(user).await {
                Ok(_) => info!("Cancelling follow to https://github.com/{}", user),
                Err(e) => error!("Error: {}", e),
            }
        } else {
            // some users have the feature of just follow others,
            // but not been follow, so, if we can reach their public followers
            // then follow the user, in other case forget the user.
            let url = format!("https://github.com/{user}?tab=followers");
            let response = reqwest::get(url).await.unwrap();
            if response.status().is_success() {
                sleep(Duration::from_millis(TIME));
                warn!("{} https://github.com/{}", custom_msg, user);
                sleep(Duration::from_millis(TIME));
                match client.users().follow(user).await {
                    Ok(_) => info!("Following to https://github.com/{}", user),
                    Err(e) => error!("Error: {}", e),
                }
            }
        }
    }
}
